<?php

declare(strict_types=1);

namespace Visma\ModuleConfig\Controller\Adminhtml\Modules;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Ui\Component\MassAction\Filter;

abstract class AbstractMassAction extends Action
{
    private const REDIRECT_URL = '*/*/grid';

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Visma_ModuleConfig::module_configuration';

    /**
     * @var Filter $filter
     */
    private $filter;

    /**
     * @param Context $context
     * @param Filter $filter
     */
    public function __construct(
        Context $context,
        Filter $filter
    ) {
        parent::__construct($context);
        $this->filter = $filter;
    }

    public function execute()
    {
        try {
            $collection = $this->filter->getCollection();

            return $this->massAction($collection);
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());

            return $this->getRedirect();
        }
    }

    /**
     * @param AbstractCollection $collection
     * @return ResultInterface
     */
    abstract protected function massAction(AbstractCollection $collection);

    /**
     * @return ResultInterface
     */
    protected function getRedirect(): ResultInterface
    {
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $redirect->setPath(self::REDIRECT_URL);
    }
}
