<?php

declare(strict_types=1);

namespace Visma\ModuleConfig\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Visma\ModuleConfig\Api\Data\ModuleInterface;

class Module extends AbstractModel implements IdentityInterface, ModuleInterface
{
    /**
     * This is the cache tag
     */
    public const CACHE_TAG = 'visma_module_config_module';

    /**
     * @inheritDoc
     */
    public function getModuleId(): ?string
    {
        return $this->getData(self::MODULE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setModuleId(string $moduleId): ModuleInterface
    {
        return $this->setData(self::MODULE_ID, $moduleId);
    }

    /**
     * @inheritDoc
     */
    public function getVendorName(): ?string
    {
        return $this->getData(self::VENDOR);
    }

    /**
     * @inheritDoc
     */
    public function setVendorName(string $vendor): ModuleInterface
    {
        return $this->setData(self::VENDOR, $vendor);
    }

    /**
     * @inheritDoc
     */
    public function getModuleName(): ?string
    {
        return $this->getData(self::MODULE);
    }

    /**
     * @inheritDoc
     */
    public function setModuleName(string $module): ModuleInterface
    {
        return $this->setData(self::MODULE, $module);
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(): bool
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setEnabled(bool $status): ModuleInterface
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getSetupVersion(): ?string
    {
        return $this->getData(self::SETUP_VERSION);
    }

    /**
     * @inheritDoc
     */
    public function setSetupVersion(?string $setupVersion): ModuleInterface
    {
        return $this->setData(self::SETUP_VERSION, $setupVersion);
    }

    /**
     * @inheritDoc
     * @return string[]
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritDoc
     */
    public function getSequence(): ?array
    {
        return $this->getData(self::SEQUENCE);
    }

    /**
     * @inheritDoc
     */
    public function setSequence(?array $sequence): ModuleInterface
    {
        return $this->setData(self::SEQUENCE, $sequence);
    }
}
