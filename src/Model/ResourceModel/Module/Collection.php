<?php

declare(strict_types=1);

namespace Visma\ModuleConfig\Model\ResourceModel\Module;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\Collection as DataCollection;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\Module\FullModuleList;
use Magento\Framework\Module\Manager;
use Visma\ModuleConfig\Api\Data\ModuleInterface;
use Visma\ModuleConfig\Api\Data\ModuleInterfaceFactory as ModuleFactory;

class Collection extends DataCollection implements SearchResultInterface
{
    private const SEPARATOR = '_';

    /**
     * Item object class name
     *
     * @var string
     */
    // phpcs:ignore
    protected $_itemObjectClass = ModuleInterface::class;

    /**
     * Collection items
     *
     * @var ModuleInterface[]
     */
    // phpcs:ignore
    protected $_items = [];

    protected $aggregations;

    protected $searchCriteria;

    protected $totalCount;

    protected $document;

    /**
     * @var FullModuleList $fullModuleList
     */
    private $fullModuleList;

    /**
     * @var Manager $manager
     */
    private $manager;

    /**
     * @var ModuleFactory $moduleFactory
     */
    private $moduleFactory;

    /**
     * @param EntityFactoryInterface $entityFactory
     * @param FullModuleList $fullModuleList
     * @param Manager $manager
     * @param ModuleFactory $moduleFactory
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        FullModuleList $fullModuleList,
        Manager $manager,
        ModuleFactory $moduleFactory
    ) {
        parent::__construct($entityFactory);
        $this->fullModuleList = $fullModuleList;
        $this->manager = $manager;
        $this->moduleFactory = $moduleFactory;
    }

    /**
     * @param bool $printQuery
     * @param bool $logQuery
     * @return Collection
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function loadData($printQuery = false, $logQuery = false): Collection
    {
        if ($this->isLoaded()) {
            return $this;
        }

        foreach ($this->getAllModules() as $key => $module) {
            list('name' => $name, 'setup_version' => $setupVersion, 'sequence' => $sequence) = $module;

            $module = $this->moduleFactory->create();
            $module->setModuleId($key)
                ->setVendorName($this->createVendorName($name))
                ->setModuleName($this->createModuleName($name))
                ->setEnabled($this->manager->isEnabled($name))
                ->setSetupVersion($setupVersion)
                ->setSequence($sequence);

            $this->addItem($module);
        }

        $this->_setIsLoaded(true);

        return $this;
    }

    /**
     * @return array
     */
    private function getAllModules(): array
    {
        return $this->fullModuleList->getAll();
    }

    /**
     * @param string $moduleName
     * @return string
     */
    private function createVendorName(string $moduleName): string
    {
        return explode(self::SEPARATOR, $moduleName)[0];
    }

    /**
     * @param string $moduleName
     * @return string
     */
    private function createModuleName(string $moduleName): string
    {
        return explode(self::SEPARATOR, $moduleName)[1];
    }

    /**
     * @inheritDoc
     */
    public function setItems(array $items = null)
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @inheritDoc
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSearchCriteria()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        // $this->searchCriteria = $searchCriteria;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTotalCount()
    {
        if (!$this->totalCount) {
            $this->totalCount = $this->getSize();
        }

        return $this->totalCount;
    }

    /**
     * @inheritDoc
     */
    public function setTotalCount($totalCount)
    {
        // $this->totalCount = $totalCount;

        return $this;
    }
}
