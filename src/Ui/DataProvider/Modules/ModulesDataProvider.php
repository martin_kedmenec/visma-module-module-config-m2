<?php

declare(strict_types=1);

namespace Visma\ModuleConfig\Ui\DataProvider\Modules;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Visma\ModuleConfig\Model\ResourceModel\Module\CollectionFactory;

class ModulesDataProvider extends AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create();
    }

    public function getData()
    {
        $collection = $this->getCollection()->toArray();

        $modifiedItems = [];
        foreach ($collection['items'] as $item) {
            $status = $item['status'] ? 'Enabled' : 'Disabled';

            $item['status'] = $status;

            $modifiedItems[] = $item;
        }

        $collection['items'] = $modifiedItems;

        return $collection;
    }
}
