<?php

declare(strict_types=1);

namespace Visma\ModuleConfig\Api\Data;

interface ModuleInterface
{
    public const MODULE_ID = 'module_id';

    public const VENDOR = 'vendor';

    public const MODULE = 'module';

    public const STATUS = 'status';

    public const SETUP_VERSION = 'setup_version';

    public const SEQUENCE = 'sequence';

    /**
     * @return string|null
     */
    public function getModuleId(): ?string;

    /**
     * @param string $moduleId
     * @return ModuleInterface
     */
    public function setModuleId(string $moduleId): ModuleInterface;

    /**
     * @return string|null
     */
    public function getVendorName(): ?string;

    /**
     * @param string $vendor
     * @return ModuleInterface
     */
    public function setVendorName(string $vendor): ModuleInterface;

    /**
     * @return string|null
     */
    public function getModuleName(): ?string;

    /**
     * @param string $module
     * @return ModuleInterface
     */
    public function setModuleName(string $module): ModuleInterface;

    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @param bool $status
     * @return ModuleInterface
     */
    public function setEnabled(bool $status): ModuleInterface;

    /**
     * @return string|null
     */
    public function getSetupVersion(): ?string;

    /**
     * @param string|null $setupVersion
     * @return ModuleInterface
     */
    public function setSetupVersion(?string $setupVersion): ModuleInterface;

    /**
     * @return array|null
     */
    public function getSequence(): ?array;

    /**
     * @param array|null $sequence
     * @return ModuleInterface
     */
    public function setSequence(?array $sequence): ModuleInterface;
}
