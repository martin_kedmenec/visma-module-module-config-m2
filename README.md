# Visma Digital Commerce: ModuleConfig

Module responsible for allowing the merchant to manually enable and disable modules from the admin app.

![Example image](doc/images/exampel.png)

## Functionality

## Technical info

PHP documentation is available [here](doc/phpdoc/index.html).

## License

This module is proprietary software belonging to Visma Digital Commerce AS.
